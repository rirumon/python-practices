import random  # For generating random number
import sys
import pygame
from pygame.locals import *

FPS = 32
SCREENWidth = 289
SCREENHEIGHT = 511
SCREEN = pygame.display.set_mode((SCREENWidth, SCREENHEIGHT))
GROUNDY = SCREENHEIGHT * 0.8
GAME_SPRITES = {}  # this is the heard
GAME_SOUNDS = {}  # this is the heard
PLAYER = 'gallery/bird.jpg'
BACKGROUND = 'gallery/background.png'
PIPE = 'gallery/pipe.jpg'


def welcomeScreen():
    pass


def mainGame():
    pass


if __name__ == '__main__':
    pygame.init()
    FPSCLOCK = pygame.time.Clock()
    pygame.display.set_caption('Flappy Bird by CodeWithHarry')
    GAME_SPRITES['numbers'] = (
        pygame.image.load('gallery/0.png').convert_alpha(),
        pygame.image.load('gallery/1.png').convert_alpha(),
        pygame.image.load('gallery/2.png').convert_alpha(),
        pygame.image.load('gallery/3.png').convert_alpha(),
        pygame.image.load('gallery/4.png').convert_alpha(),
        pygame.image.load('gallery/5.png').convert_alpha(),
        pygame.image.load('gallery/6.png').convert_alpha(),
        pygame.image.load('gallery/7.png').convert_alpha(),
        pygame.image.load('gallery/8.png').convert_alpha(),
        pygame.image.load('gallery/9.png').convert_alpha(),
    )

    GAME_SPRITES['message'] = pygame.image.load('gallery/message.png').convert_alpha()
    GAME_SPRITES['base'] = pygame.image.load('gallery/base.jpg').convert_alpha()
    GAME_SPRITES['pipe'] = (pygame.transform.rotate(pygame.image.load(PIPE).convert_alpha(), 180),
                            pygame.image.load(PIPE).convert_alpha()
                            )
    GAME_SPRITES['message'] = pygame.image.load('gallery/message.png').convert_alpha()
    GAME_SPRITES['base'] = pygame.image.load('gallery/base.jpg').convert_alpha()
    GAME_SPRITES['pipe'] = (pygame.transform.rotate(pygame.image.load(PIPE).convert_alpha(), 180),
                            pygame.image.load(PIPE).convert_alpha()
                            )

    # Game sounds
    GAME_SOUNDS['die'] = pygame.mixer.Sound('gallery/1.mp3')
    GAME_SOUNDS['hit'] = pygame.mixer.Sound('gallery/2.mp3')
    GAME_SOUNDS['point'] = pygame.mixer.Sound('gallery/3.mp3')
    GAME_SOUNDS['swoosh'] = pygame.mixer.Sound('gallery/4.mp3')
    GAME_SOUNDS['wing'] = pygame.mixer.Sound('gallery/1.mp3')

    GAME_SPRITES['background'] = pygame.image.load(BACKGROUND).convert()
    GAME_SPRITES['player'] = pygame.image.load(PLAYER).convert_alpha()

    while True:
        welcomeScreen()  # Shows welcome screen to the user until he presses a button
        mainGame()  # This is the main game function
