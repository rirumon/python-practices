import pyttsx3
import pythoncom
import datetime
import speech_recognition as sr

# from django.conf.locale import sr

engine = pyttsx3.init('sapi5')
voices = engine.getProperty('voices')
# print(voices[1].id)
engine.setProperty('voices', voices[1].id)


def speak(audio):
    engine.say(audio)
    engine.runAndWait()


def wishMe():
    hour = int(datetime.datetime.now().hour)
    if 0 <= hour < 12:
        speak('Good Morning')
    elif 12 <= hour < 18:
        speak("Good AfterNoon!")
    else:
        speak("Good evening")
    speak("I Am Natok vai, Ke Koiben Kon")


def takeAudioInput():
    r = sr.Recognizer
    with sr.Microphone as source:
        print('Listening...')
        r.pause_threshold = 1
        audio = r.listen(source)
    try:
        print('Recognizing...')
        quary = r.recognize_google(audio, Language="en-us")
        print(f"User said: {quary}\n")
    except Exception as e:
        print("Say that again please...")
        return "none"
    return quary

if __name__ == '__main__':
    wishMe()
    takeAudioInput()
