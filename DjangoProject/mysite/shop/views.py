from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def index(request):
    return render(request,'shop/index.html')

def about(request):
    return HttpResponse("About")

def contact(request):
    return HttpResponse("contact")

def product(request):
    return HttpResponse("product")