
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="Shop Home"),
    path('about/', views.about, name="Shop About"),
    path('contact/', views.contact, name="Shop Contact"),
    path('product/', views.product, name="Shop Product"),
]