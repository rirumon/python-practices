import time

from bs4 import BeautifulSoup
from plyer import notification
import requests


def notifyMe(title, message):
    notification.notify(
        title=title,
        message=message,
        app_icon="icon.ico",
        timeout=10
    )


def getData(url):
    res = requests.get(url)
    return res.text


if __name__ == '__main__':
    # https://www.worldometers.info/coronavirus/
    while True:
        htmlData = getData('https://www.worldometers.info/coronavirus/')
        soup = BeautifulSoup(htmlData, 'html.parser')
        dataStr = ""
        for tr in soup.find_all('tr'):
            dataStr += tr.get_text()

        itemList = dataStr.split("\n\n")
        print(itemList)

        # key = ['Total:']
        for item in itemList[0:]:
            dataList = item.split("\n")
            print(dataList)
            if dataList[-1] in 'Total:':
                nTitle = "Total Case History Of Covid-19"
                nText = f"Total Cases : {dataList[1]} \n" \
                        f"New Cases : {dataList[2]} \n" \
                        f"Total Deaths : {dataList[3]} \n" \
                        f"Total Recovered : {dataList[4]} \n" \
                        f"Active Cases : {dataList[5]} \n" \
                        f"Critical {dataList[6]} \n" \
                        f"Tot Cases : {dataList[7]} \n"
                notifyMe(nTitle, nText)
                time.sleep(2)
        time.sleep(1300)
        # print(dataList)
    # print(len(dataList))
    # print(soup)
    # notifyMe('Rumon', "Fuck your mind")
